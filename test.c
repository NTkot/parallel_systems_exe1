#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include "mmio/mmio.h"
#include "utils.h"
#include "triangle_detection_seq.h"

int main() {
    // int test_array[5] = {0, 1, 2, 4, 5};
    // print_int_vector(test_array, 5);

    // int I[3] = {1, 2, 3};
    // int J[3] = {0, 1, 1};
    // int nnz = 3, cols = 4;
    // int J_csc[5] = {0, 0, 0, 0, 0};
    // int I_csc[6] = {0, 0, 0, 0, 0, 0};

    // int I[5] = {1, 3, 3, 4, 5};
    // int J[5] = {0, 0, 2, 1, 3};
    // int nnz = 5, cols = 6;
    // int J_csc[7] = {0, 0, 0, 0, 0, 0};
    // int I_csc[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    // int I[6] = {1, 2, 3, 2, 3, 3};
    // int J[6] = {0, 0, 0, 1, 1, 2};
    // int nnz = 6, cols = 4;
    // int J_csc[5] = {0, 0, 0, 0, 0};
    // int I_csc[12] = {0, 0, 0, 0, 0, 0, 0, 0};

    int I[5] = {1, 2, 3, 2, 3};
    int J[5] = {0, 0, 0, 1, 2};
    int nnz = 5, cols = 4;
    int J_csc[5] = {0, 0, 0, 0, 0};
    int I_csc[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    // int I[4] = {1, 2, 3, 2};
    // int J[4] = {0, 0, 0, 1} ;
    // int nnz = 4, cols = 4;
    // int J_csc[5] = {0, 0, 0, 0, 0};
    // int I_csc[8] = {0, 0, 0, 0, 0, 0, 0, 0};

    // int I[7] = {1, 3, 4, 2, 4, 4, 4};
    // int J[7] = {0, 0, 0, 1, 1, 2, 3};
    // int nnz = 7, cols = 5;
    // int J_csc[6] = {0, 0, 0, 0, 0};
    // int I_csc[14] = {0, 0, 0, 0, 0, 0, 0, 0};

    mm_symmetrical_to_csc(I, J, I_csc, J_csc, nnz, cols);
    printf("-------- A Matrix --------\n");
    print_int_vector(I_csc, 2*nnz);
    print_int_vector(J_csc, cols+1);

    int *C_vals, *c_vector, total_triangles;
    C_vals   = (int *)calloc(2*nnz, sizeof(int));
    c_vector = (int *)malloc(cols * sizeof(int));
    count_triangles_seq(I_csc, J_csc, C_vals, cols);

    csc_expand_symmetrical(I_csc, J_csc, C_vals, cols);

    count_vertices_triangles_seq(J_csc, C_vals, c_vector, cols);
    total_triangles = count_total_triangles_seq(c_vector, cols);

    printf("\n-------- C Matrix --------\n");
    print_int_vector(C_vals, 2*nnz);
    print_int_vector(c_vector, cols);
    printf("Total triangles in graph = %d\n", total_triangles);

    return 0;
}