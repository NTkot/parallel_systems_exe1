#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "triangle_detection_omp.h"

#define COUNT_TRIANGLES_COLS_THREADS 12
#define COUNT_TRIANGLES_ROWS_THREADS 1
#define COUNT_TRIANGLES_KTH_THREADS 1

// Function that returns number of total triangles in a graph
int count_total_triangles_omp(const int *c_vector, const int cols) {
    int sum = 0;
    for(int i = 0; i < cols; i++) {
        sum += c_vector[i];
    }
    if(sum % 3 != 0) {
        printf("PROBLEM IN count_total_triangles_sequential\n");
        return -1;
    }
    else {
        return (sum / 3);
    }
}

// Function that calculates a vector whose i-th position represents the total number of triangles the i-th vertex takes part in
void count_vertices_triangles_omp(const int *C_cols_indices, const int *C_vals, int *c_vector, const int cols) {
    for(int j = 0; j < cols; j++) {
        c_vector[j] = 0;
        if(C_cols_indices[j+1] - C_cols_indices[j] == 0)
            continue;

        for(int i = C_cols_indices[j]; i < C_cols_indices[j+1]; i++)
            c_vector[j] += C_vals[i];

        if(c_vector[j] % 2 != 0) {
            printf("PROBLEM IN count_vertices_triangles_sequential in %d index\n", j);
            return;
        }
        else
            c_vector[j] /= 2;
    }
}

// Function that calculates the requested matrix C in CSC format and returns the number of non-zero values in C matrix
void count_triangles_omp(const int* A_rows, const int *A_cols_indices, int *C_vals, const int cols) {
    omp_lock_t C_vals_lock;
    omp_init_lock(&C_vals_lock);

    // If threads chosen in this for-loop are only 1, no need to use OpenMP
    #if COUNT_TRIANGLES_COLS_THREADS!=1
        #pragma omp parallel for num_threads(COUNT_TRIANGLES_COLS_THREADS) shared(A_rows, A_cols_indices, C_vals, cols) schedule(dynamic)
    #endif
    for(int j = 0; j < cols; j++) {
        if(A_cols_indices[j+1] - A_cols_indices[j] == 0)
            continue;

        int i;
        // If threads chosen in this for-loop are only 1, no need to use OpenMP
        #if COUNT_TRIANGLES_ROWS_THREADS!=1
            #pragma omp parallel for num_threads(COUNT_TRIANGLES_ROWS_THREADS) shared(A_rows, A_cols_indices, C_vals, cols) private(i) schedule(dynamic)
        #endif
        for(int i_index = A_cols_indices[j]; i_index < A_cols_indices[j+1]; i_index++) {
            i = A_rows[i_index];
            if(j >= i)
                continue;
            
            int k;
            
            // If threads chosen in this for-loop are only 1, no need to use OpenMP
            #if COUNT_TRIANGLES_KTH_THREADS!=1
                #pragma omp parallel for num_threads(COUNT_TRIANGLES_KTH_THREADS) shared(A_rows, A_cols_indices, C_vals, cols) private(k)
            #endif
            for(int k_index = A_cols_indices[i]; k_index < A_cols_indices[i+1]; k_index++) {
                k = A_rows[k_index];
                if(find_element_csc_omp(A_rows, A_cols_indices, j, k)) {
                    omp_set_lock(&C_vals_lock);
                    C_vals[i_index]++;
                    omp_unset_lock(&C_vals_lock);
                }
            }
        }
    }
}

// Function that searches a CSC formatted matrix for a specific element and if that element is non-zero, it returns 1, else it returns 0.
int find_element_csc_omp(const int* A_rows, const int *A_cols_indices, const int i, const int j) {
    if(A_cols_indices[j+1] - A_cols_indices[j] == 0)
        return 0;
    for(int row_index = A_cols_indices[j]; row_index < A_cols_indices[j+1]; row_index++) {
        if(A_rows[row_index] == i)
            return 1;
    }
    return 0;
}
