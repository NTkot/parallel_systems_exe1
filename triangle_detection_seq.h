#ifndef TRIANGLE_DETECTION_SEQ_H
#define TRIANGLE_DETECTION_SEQ_H

void count_vertices_triangles_seq(const int *C_cols_indices, const int *C_vals, int *c_vector, const int cols);
int count_total_triangles_seq(const int *c_vector, const int cols);
void count_triangles_seq(const int* A_rows, const int *A_cols_indices, int *C_vals, const int cols);
int find_element_csc_seq(const int* I_csc, const int *J_csc, const int i, const int j);

#endif