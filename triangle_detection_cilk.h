#ifndef TRIANGLE_DETECTION_CILK_H
#define TRIANGLE_DETECTION_CILK_H

void count_vertices_triangles_cilk(const int *C_cols_indices, const int *C_vals, int *c_vector, const int cols);
int count_total_triangles_cilk(const int *c_vector, const int cols);
void count_triangles_cilk(const int* A_rows, const int *A_cols_indices, int *C_vals, const int cols);
int find_element_csc_cilk(const int* I_csc, const int *J_csc, const int i, const int j);

#endif