#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include "utils.h"
#include "triangle_detection_omp.h"

#define REPETITIONS 3   // Number of times to execute the pipeline over the same dataset, to get a better sample of execution time

int main(int argc, char *argv[]) {
    FILE *f;
    int rows, cols, nnz, total_triangles;
    int *A_I, *A_J, *A_rows, *A_cols_indices, *C_vals, *c_vector;
    struct timeval start, end;
    
    // Open Matrix Market file for reading
    if ((f = fopen("datasets/com-Youtube/com-Youtube.mtx", "r")) == NULL) {
        printf("File read failed\n");
        exit(1);
    }
    read_mm_metadata(f, &rows, &cols, &nnz); // Function that reads metadata of MM formatted file
    // If matrix is not square, then exit
    if(rows != cols) {
        printf("Matrix entered is not square");
        return 4;
    }

    // A_I/A_J will contain coordinates of the non-zero elements of the matrix (COO format)
    A_I = (int *)malloc(nnz   * sizeof(int));
    A_J = (int *)malloc(nnz   * sizeof(int));
    if(A_I == NULL || A_J == NULL) return 1;

    read_mm(f, A_I, A_J, nnz);  // Function that reads the MM data from file and stores coordinates in A_I and A_J vectors (COO format)
    fclose(f);                  // Close file
    
    long elapsed_time;
    float mean = 0;
    printf("\n\n");
    for(int i = 0; i < REPETITIONS; i++) {
        printf("Iteration: %d\n", i);
        
        // A_rows/A_cols_indices are the corresponding vectors of a CSC formatted matrix
        A_rows         = (int *)calloc(2*nnz,  sizeof(int));
        A_cols_indices = (int *)calloc(cols+1, sizeof(int));
        if(A_rows == NULL || A_cols_indices == NULL) return 1;

        
        gettimeofday(&start, NULL); // Start counting

        // Function that converts a MM symmetric matrix (only lower triangle stored) to a complete CSC matrix
        mm_symmetrical_to_csc(A_I, A_J,
                              A_rows, A_cols_indices, 
                              nnz, cols);

        C_vals = (int *)calloc(2*nnz, sizeof(int));     // C = A .* (A * A) has the same CSC vectors as the adjacency matrix with the additions of a values vector
        if(C_vals == NULL) return 1;

        count_triangles_omp(A_rows, A_cols_indices, C_vals, cols);      // Compute triangles only for lower triangle of A in parallel execution using OpenMP
        csc_expand_symmetrical(A_rows, A_cols_indices, C_vals, cols);   // C is symmetric, so copy lower triangle to upper triangle
        
        c_vector = (int *)malloc(cols * sizeof(int));   // c_vector is a size=n vector whose i-th element indicates the number of triangles the i-th vertex takes part in
        if(c_vector == NULL) return 1;
        count_vertices_triangles_omp(A_cols_indices, C_vals, c_vector, cols);   // Sequentially calculate c_vector
        total_triangles = count_total_triangles_omp(c_vector, cols);            // Sequentially calculate total number of triangles

        gettimeofday(&end, NULL);   // End counting
        elapsed_time = (end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec); // Elapsed time counted in us
        mean += (float)elapsed_time;
        printf("Total triangles: %d\n", total_triangles); // Print total number of triangles
        printf("Elapsed time: %ldus\n\n", elapsed_time);  // Print time elapsed in microsends
        
        free(A_rows);
        free(A_cols_indices);
        free(C_vals);
        free(c_vector);
    }
    mean /= 1000000*REPETITIONS;     // Average out result to seconds
    printf("Average time: %.4fs\n", mean);

    free(A_I);
    free(A_J);

    return 0;
}
