COMPILER=gcc
CILK_COMPILER=gcc-6 # GCC-6 needed for Cilk Plus, https://stackoverflow.com/questions/67280779/cilk-h-no-such-file-or-directory
FLAGS=-O2

all: main main_omp main_cilk main_pth


# Linking executables
main: main.o utils.o mmio.o triangle_detection_seq.o
	${COMPILER} ${FLAGS} mmio.o utils.o triangle_detection_seq.o main.o -o main

main_omp: main_omp.o utils.o mmio.o triangle_detection_omp.o
	${COMPILER} ${FLAGS} -fopenmp mmio.o utils.o triangle_detection_omp.o main_omp.o -o main_omp

main_cilk: main_cilk.o cilk-utils.o cilk-mmio.o triangle_detection_cilk.o
	${CILK_COMPILER} ${FLAGS} -fcilkplus cilk-utils.o cilk-mmio.o triangle_detection_cilk.o main_cilk.o -o main_cilk

main_pth: main_pth.o utils.o mmio.o triangle_detection_pth.o
	${COMPILER} ${FLAGS} -pthread utils.o mmio.o triangle_detection_pth.o main_pth.o -o main_pth


# Executable-objects compiling
main.o: main.c
	${COMPILER} -c ${FLAGS} main.c

main_omp.o: main_omp.c
	${COMPILER} -c ${FLAGS} -fopenmp main_omp.c

main_cilk.o: main_cilk.c
	${CILK_COMPILER} -c ${FLAGS} -fcilkplus main_cilk.c

main_pth.o: main_pth.c
	${COMPILER} -c ${FLAGS} -pthread main_pth.c


# Algorithm implementations source files compiling
triangle_detection_seq.o: triangle_detection_seq.c
	${COMPILER} -c ${FLAGS} triangle_detection_seq.c

triangle_detection_omp.o: triangle_detection_omp.c
	${COMPILER} -c ${FLAGS} -fopenmp triangle_detection_omp.c

triangle_detection_cilk.o: triangle_detection_cilk.c
	${CILK_COMPILER} -c ${FLAGS} -fcilkplus triangle_detection_cilk.c

triangle_detection_pth.o: triangle_detection_pth.c
	${COMPILER} -c ${FLAGS} -pthread triangle_detection_pth.c


# Libraries compiling
mmio.o: mmio/mmio.c
	${COMPILER} -c ${FLAGS} mmio/mmio.c

cilk-mmio.o: mmio/mmio.c
	${CILK_COMPILER} -c ${FLAGS} mmio/mmio.c -o cilk-mmio.o

utils.o: utils.c
	${COMPILER} -c ${FLAGS} utils.c

cilk-utils.o: utils.c
	${CILK_COMPILER} -c ${FLAGS} utils.c -o cilk-utils.o


# Test files
test: test.o utils.o mmio.o triangle_detection_seq.o
	${COMPILER} ${FLAGS} mmio.o utils.o triangle_detection_seq.o test.o -o test

test.o: test.c
	${COMPILER} -c ${FLAGS} test.c

# test_omp.o: test_omp.c
# 	${COMPILER} -c ${FLAGS} test_omp.c

# test_omp: test_omp.o utils.o mmio.o triangle_detection_omp.o
# 	${COMPILER} ${FLAGS} -fopenmp mmio.o utils.o triangle_detection_omp.o test_omp.o -o test_omp

clean:
	rm *.o main main_omp main_cilk main_pth test test_omp