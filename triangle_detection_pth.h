#ifndef TRIANGLE_DETECTION_PTHREADS_H
#define TRIANGLE_DETECTION_PTHREADS_H

typedef struct {
    const int *A_rows, *A_cols_indices;
    int *C_vals;
    int cols;
} thread_arg_col;

void count_vertices_triangles_pth(const int *C_cols_indices, const int *C_vals, int *c_vector, const int cols);
void *thread_col_calculate_work(void *col_arg);
int count_total_triangles_pth(const int *c_vector, const int cols);
void count_triangles_pth(const int* A_rows, const int *A_cols_indices, int *C_vals, const int cols);
int find_element_csc_pth(const int* I_csc, const int *J_csc, const int i, const int j);

#endif