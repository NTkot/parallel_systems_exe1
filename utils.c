#include <stdio.h>
#include <stdlib.h>
#include "mmio/mmio.h"
#include "utils.h"

// Function that reads number of rows, columns and non-zero elements from Matrix Market file
void read_mm_metadata(FILE *f, int *no_rows, int *no_cols, int *nnz) {
    MM_typecode code;

    if (mm_read_banner(f, &code) != 0) {
        printf("Could not process Matrix Market banner\n");
        exit(2);
    }

    if ((mm_read_mtx_crd_size(f, no_rows, no_cols, nnz)) !=0)
        exit(3);

    printf("MM_typecode = ");
    for(int i = 0; i < 4; i++)
        printf("%c ", code[i]);
    printf("\n");

    printf("Rows     = %d\nColumns  = %d\nNon-zero = %d\n", *no_rows, *no_cols, *nnz);
}

// Function that reads COO-formatted MM matrix. Assumes the matrix is an adjacency matrix for a graph (consists of 0's and 1's). 
// It, also, assumes 1-based indexing and converts the resulting ROW (I) and COLUMN (J) vectors to 0-based indexing
void read_mm(FILE *f, int *I, int *J, int nnz) {
    for (int i=0; i < nnz; i++) {
        fscanf(f, "%d %d\n", &I[i], &J[i]);
        I[i]--;
        J[i]--;
    }
}

// Converts an MM symmetrical matrix (only Graph Adjacency matrices) to a CSC matrix. The resulting 
// vectors contain all elements (and not just the lower tringular matrix segment, as MM does).
void mm_symmetrical_to_csc(const int *I, const int *J, int *I_csc, int *J_csc, int nnz, int cols) {
    int* J_total_nnz = (int *)calloc(cols, sizeof(int));
    int i,j;

    for(int k = 0; k < nnz; k++) {
        i = I[k];
        j = J[k];
        J_total_nnz[j]++;
        J_total_nnz[i]++;
    }

    int sum = 0;
    for(int k = 0; k < cols; k++) {
        J_csc[k] = sum;
        sum += J_total_nnz[k];
        J_total_nnz[k] = 0;
    }
    J_csc[cols] = sum;

    for(int k = 0; k < nnz; k++) {
        i = I[k];
        j = J[k];
        I_csc[J_csc[i] + J_total_nnz[i]++] = j;
        I_csc[J_csc[j] + J_total_nnz[j]++] = i;
    }

    free(J_total_nnz);

    // J_csc[0] = 0;
    // int current_nnz = 0;
    // for(int c = 0; c < cols; c++) {
    //     for(int k = 0; k < nnz; k++) {
    //         if(J[k] == c) {
    //             I_csc[current_nnz] = I[k];
    //             current_nnz++;
    //         }
    //         else if(I[k] == c) {
    //             I_csc[current_nnz] = J[k];
    //             current_nnz++;
    //         }
    //     }
    //     J_csc[c+1] = current_nnz;
    // }
}

// Expands a CSC symmetrical matrix (with its upper triangular consisting of 0s) to the equivalent symmetric matrix
void csc_expand_symmetrical(const int *C_rows, const int *C_cols_indices, int *C_vals, int cols) {
    int i;
    for(int j = 0; j < cols; j++) {
        if(C_cols_indices[j+1] - C_cols_indices[j] == 0)
            continue;
        
        for(int row_index = C_cols_indices[j]; row_index < C_cols_indices[j+1]; row_index++) {
            i = C_rows[row_index];
            if(j >= i)
                continue;
            for(int index = C_cols_indices[i]; index < C_cols_indices[i+1]; index++) {
                if(C_rows[index] == j) {
                    C_vals[index] = C_vals[row_index];
                }
            }
        }
    }
}

// Function that prints a vector, given its size
void print_int_vector(int *vector, int size) {
    printf("[");
    for(int i = 0; i < size; i++) {
        printf("%d ", vector[i]);
    }
    printf("\b]\n");
}