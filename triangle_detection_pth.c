#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "triangle_detection_pth.h"

#define THREADS 12

int next_col;
pthread_mutex_t next_col_lock;
pthread_mutex_t C_vals_lock;

// Function that calculates the requested matrix C in CSC format and returns the number of non-zero values in C matrix
void count_triangles_pth(const int* A_rows, const int *A_cols_indices, int *C_vals, const int cols) {
    next_col = 0;
    pthread_mutex_init(&next_col_lock, NULL);
    pthread_mutex_init(&C_vals_lock, NULL);

    pthread_t *threads;
    pthread_attr_t pthread_attr;
    thread_arg_col *col_args;

    threads = (pthread_t *)malloc(THREADS * sizeof(*threads));
    pthread_attr_init(&pthread_attr);
    col_args = (thread_arg_col *)malloc(THREADS * sizeof(thread_arg_col));

    for (int i = 0; i < THREADS; i++) {
        col_args[i].A_rows = A_rows;
        col_args[i].A_cols_indices = A_cols_indices;
        col_args[i].C_vals = C_vals;
        col_args[i].cols = cols;
        pthread_create(&threads[i], &pthread_attr, thread_col_calculate_work, (void *)(&col_args[i]));
    }

    for (int i = 0; i < THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
}

// Function that represents work of a single thread
void *thread_col_calculate_work(void *arg) {
    thread_arg_col *col_arg = (thread_arg_col *)arg;
    int j, i, k;

    while(1) {
        // If threads used are only one, then no need to lose time on mutex (not needed)
        #if THREADS!=1
        pthread_mutex_lock(&next_col_lock);
        if(next_col >= col_arg->cols) {
            pthread_mutex_unlock(&next_col_lock);
            pthread_exit(NULL);
        }
        j = next_col;
        next_col++;
        pthread_mutex_unlock(&next_col_lock);
        #else
        if(next_col >= col_arg->cols)
            pthread_exit(NULL);
        j = next_col;
        next_col++;
        #endif

        if(col_arg->A_cols_indices[j+1] - col_arg->A_cols_indices[j] == 0)
            continue;

        for(int i_index = col_arg->A_cols_indices[j]; i_index < col_arg->A_cols_indices[j+1]; i_index++) {
            i = col_arg->A_rows[i_index];
            if(j >= i)
                continue;
            for(int k_index = col_arg->A_cols_indices[i]; k_index < col_arg->A_cols_indices[i+1]; k_index++) {
                k = col_arg->A_rows[k_index];
                if(find_element_csc_pth(col_arg->A_rows, col_arg->A_cols_indices, j, k)) {
                    pthread_mutex_lock(&C_vals_lock);
                    col_arg->C_vals[i_index]++;
                    pthread_mutex_unlock(&C_vals_lock);
                }
            }
        }
    }
}

// Function that returns number of total triangles in a graph
int count_total_triangles_pth(const int *c_vector, const int cols) {
    int sum = 0;
    for(int i = 0; i < cols; i++) {
        sum += c_vector[i];
    }
    if(sum % 3 != 0) {
        printf("PROBLEM IN count_total_triangles_pth\n");
        return -1;
    }
    else {
        return (sum / 3);
    }
}

// Function that calculates a vector whose i-th position represents the total number of triangles the i-th vertex takes part in
void count_vertices_triangles_pth(const int *C_cols_indices, const int *C_vals, int *c_vector, const int cols) {
    for(int j = 0; j < cols; j++) {
        c_vector[j] = 0;
        if(C_cols_indices[j+1] - C_cols_indices[j] == 0)
            continue;

        for(int i = C_cols_indices[j]; i < C_cols_indices[j+1]; i++)
            c_vector[j] += C_vals[i];

        if(c_vector[j] % 2 != 0) {
            printf("PROBLEM IN count_vertices_triangles_pth in %d index\n", j);
            return;
        }
        else
            c_vector[j] /= 2;
    }
}

// Function that searches a CSC formatted matrix for a specific element and if that element is non-zero, it returns 1, else it returns 0.
int find_element_csc_pth(const int* A_rows, const int *A_cols_indices, const int i, const int j) {
    if(A_cols_indices[j+1] - A_cols_indices[j] == 0)
        return 0;
    for(int row_index = A_cols_indices[j]; row_index < A_cols_indices[j+1]; row_index++) {
        if(A_rows[row_index] == i)
            return 1;
    }
    return 0;
}
