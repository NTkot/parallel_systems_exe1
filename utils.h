#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>

void read_mm_metadata(FILE *f, int *no_rows, int *no_cols, int *nnz);
void read_mm(FILE *f, int *I, int *J, int nnz);
void mm_symmetrical_to_csc(const int *I, const int *J, int *I_csc, int *J_csc, int nnz, int cols);
void csc_expand_symmetrical(const int *C_rows, const int *C_cols_indices, int *C_vals, int cols);
void print_int_vector(int *vector, int size);

#endif